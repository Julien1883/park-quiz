package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        Park park = createPark();

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(parkDatafilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {
            Field[] declaredFields = Park.class.getDeclaredFields();
            while (bufferedReader.readLine() != null) {
                String str = bufferedReader.readLine();
                String s = str.substring(1, str.indexOf(":"));
                for (Field field : declaredFields) {
                    if (field.getName().equals(s)) {
                        Field declaredField = Park.class.getDeclaredField(s);
                        declaredField.setAccessible(true);
                        String set =str.substring(str.indexOf(":") + 2, str.length() - 1);
                        if(set==null){
                            set="null";
                        }
                        if (!checkAnnotation(declaredField, set)) {
                            declaredField.set(park, set);
                        } else {
                            BufferedReader temp = new BufferedReader(fileReader);
                            while (temp.readLine() != null) {
                                String t = bufferedReader.readLine();
                                set =t.substring(t.indexOf(":") + 2, t.length() - 1);
                                if(set==null){
                                    set="null";
                                }
                                if (t.substring(1, t.indexOf(":")).
                                        equals(declaredField.getAnnotation(FieldName.class).value())) {
                                    declaredField.set(park, set);
                                }
                            }
                        }
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        //write your code here
        return null;
    }



    private Park createPark() {
        Constructor<Park> declaredConstructor = null;
        try {
            declaredConstructor = Park.class.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        declaredConstructor.setAccessible(true);
        Park park = null;
        try {
            park = declaredConstructor.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return park;
    }

    private boolean checkAnnotation(Field declaredField, String substring) {
        boolean fl = false;

        List<ParkParsingException.ParkValidationError> validationErrors = null;
        if (declaredField.isAnnotationPresent(NotBlank.class)) {
            if (substring.equals("null"))
                validationErrors.add(new ParkParsingException.ParkValidationError(
                        declaredField.getName(), " null там, где его не должно быть "));
        }


        if (declaredField.isAnnotationPresent(MaxLength.class)) {
            if (substring.length() > declaredField.getAnnotation(MaxLength.class).value()) {
                validationErrors.add(new ParkParsingException.ParkValidationError(
                        declaredField.getName(), " превышен лимит символов "));
            }


            if (declaredField.isAnnotationPresent(FieldName.class)) {
                fl = true;
            }
        }


        if (validationErrors != null) {
            throw new ParkParsingException(" выдал ошибку... ", validationErrors);
        }
        return fl;
    }

}